#!/usr/bin/env python3

"""Convert a PDF to CSV format."""

from pathlib import Path
from pprint import pprint

import pymsgbox
import PyPDF2

from IPython.core.debugger import set_trace


def input_pdf_name() -> Path:
    """Get PDF file name from user and confirm input. Input request
    loops until user confirms.
    :return PDF file name as :type Path."""
    while True:  # Confirm input
        pdf_path = pymsgbox.prompt("Enter the name of the PDF file")
        if pymsgbox.confirm(f"Continue with '{pdf_path}'?") == "OK":
            break
    return Path(pdf_path)



def pdf_pages(pdf_file: (Path, str)) -> dict:
    """Extract text from 'pdf_file'.
    :return text for each page as a dict"""
    # Caste 'pdf_file' as Path and resolve, to be sure
    pdf_file = Path(pdf_file).resolve()
    pages = dict()
    # Open PDF and get a pdf reader
    with open(pdf_file, 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)
        # Get the first page and extract the text
        for page_number in range(pdf_reader.numPages):
            page = pdf_reader.getPage(page_number)
            # Extract text, and update 'pages' dict
            pages.update({page_number: page.extractText()})
    return pages


def parse_text(pages: dict) -> dict:
    """
    """
    """
    # Split the text block up by newlines
    memo_lines = [line for line in memo.split("\n")]
    # Split the lines by tabs
    csv_memo = [line.split("\t") for line in memo_lines]
    # Filter out blank and whitespace only strings
    f_memo = [x for line in csv_memo for x in line if x.split()]
    pprint(f_memo)
    """
    pass


def write_pages(pages: dict, file_name: (Path, str)) -> int:
    """Write 'pages' to a text file as 'file_name'.
    Raise OSError if 'file_name' exists."""
    # Ensure 'file_name' is of type 'Path'
    file_name = Path(file_name).resolve()
    # Raise exception if 'file_name' exists
    if file_name.exists():
        raise FileExistsError(f"File {file_name} already exists!")
    with open(file_name, 'w') as f:
        for page in pages.values():
            for chunk in page:
                f.write(chunk)
    return 0


def hold_pprint(pages: dict) -> int:
    """Use 'pprint' module to print 'pages', then use 'input' function
    to hold the terminal and text open."""
    pprint(pages)
    input("Extracted Text...")
    return 0


def main() -> int:
    while True:  # Allow user to repeat for multiple files
        pdf_path = Path("data").resolve() / input_pdf_name()
        pages = pdf_pages(pdf_path)
        set_trace()
        # write_pages(pages, input_pdf_name())
        # hold_pprint(pages)
        if pymsgbox.confirm("Run for another file?") != "OK":
            break
    return 0


if __name__ == "__main__":
    main()
